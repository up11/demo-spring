pipeline {
    agent any

    stages {
        stage('Deploy with ssh') {
            steps {
                script {
                def remote = [:]
				remote.name = 'demo'
				remote.host = ''
				remote.user = ''
				remote.password = ''
				remote.allowAnyHosts = true
                sshCommand remote: remote, command: 'docker container run -d somkiat/spring-api:1.0'
                }
            }
        }
    }
}
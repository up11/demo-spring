## Step to run with Apache Maven 2023
```
$mvnw clean package
$java -jar target/demo-jar-0.0.1-SNAPSHOT.jar
$mvnw test
$mvnw clean package -Dmaven.test.skip=true
```

## Step to run with Docker
```
$git clone https://gitlab.com/up11/demo-spring.git demo
$cd demo

// Build image
$docker image build -t api:1.0 -f Dockerfile .

// Run container
$docker container run -d -p 8080:8080 api:1.0
```

## API testing with newman (Postman)
```
$npm install -g newman
$newman run day01.postman_collection.json -r cli,junit
```

## Working with SonarQube
```
$mvnw sonar:sonar \
 -Dsonar.host.url=http://<ip>:9000 \
 -Dsonar.projectKey=<key> \
 -Dsonar.projectName=<name> \
 -Dsonar.login=<user> \
 -Dsonar.password=<password> \
 -Dsonar.java.binaries=target/classes
```

## Working with [Dependency check](https://jeremylong.github.io/DependencyCheck/dependency-check-maven/)
```
$mvnw verify
$mvnw site
```

## Working with Docker compose

Create JAR file of Example application
```
$mvnw clean package -Dmaven.test.skip=true
```

Build and Create containers
```
$docker-compose build

$docker-compose up -d
$docker-compose ps

NAME                COMMAND                  SERVICE             STATUS              PORTS
demo-spring-api-1   "java -jar demo.jar"     api                 running             0.0.0.0:9090->8080/tcp
demo-spring-db-1    "docker-entrypoint.s…"   db                  running (healthy)   0.0.0.0:3306->3306/tcp, 33060/tcp

$ docker-compose logs --follow
```

Testing API with URLs
- http://localhost:9090/message/1
- http://localhost:9090/message/2
- http://localhost:9090/message/3


## Resources
* Jenkins plugins
  * [SSH Pipeline Steps](https://plugins.jenkins.io/ssh-steps/)  
* [Vulnerability Scanning Tools](https://owasp.org/www-community/Vulnerability_Scanning_Tools)

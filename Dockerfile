# FROM openjdk:8u312-jdk-slim-buster as build
# WORKDIR /source
# COPY . .
# RUN ./mvnw package

# FROM openjdk:8u312-jre-slim-buster
# WORKDIR /app
# COPY --from=build  /source/target/demo-jar-0.0.1-SNAPSHOT.jar /app/demo.jar
# EXPOSE 8080
# CMD [ "java", "-jar", "demo.jar" ]

FROM openjdk:8u312-jre-slim-buster
WORKDIR /app
RUN apt-get update && apt-get install curl -y
COPY target/demo-jar-0.0.1-SNAPSHOT.jar /app/demo.jar
EXPOSE 8080
CMD [ "java", "-jar", "demo.jar" ]
use db_example;

CREATE TABLE my_message(
   id serial PRIMARY KEY,
   message VARCHAR (50) UNIQUE NOT NULL
);
#!/bin/bash

docker-compose down

echo "Starting api"

docker-compose up -d api

STATUS="starting"

while [ "$STATUS" != "healthy" ]
do
    STATUS=$(docker inspect --format {{.State.Health.Status}} api)
    echo "demo_api state = $STATUS"
    sleep 5
done

echo "API is running"

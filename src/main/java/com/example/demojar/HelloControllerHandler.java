package com.example.demojar;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class HelloControllerHandler {

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorResponse> notFound(Exception e) {
        ErrorResponse response = new ErrorResponse(404, "Data not found");
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

}

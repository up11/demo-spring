package com.example.demojar;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<MyMessage,
        Integer> {
}

package com.example.demojar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Autowired
    private MessageRepository messageRepository;

    @GetMapping("/message/{id}")
    public MyResponse getDataById(@PathVariable String id) {
        MyMessage myMessage = messageRepository.getById(Integer.parseInt(id));
        return new MyResponse(myMessage.getId(), myMessage.getMessage());
    }

}
